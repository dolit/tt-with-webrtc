<?php if (!defined('THINK_PATH')) exit();?>
<div class="pageContent">
	<form method="post" action="__URL__/update/navTabId/__MODULE__" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxDone);">
		<input  name="id" type="hidden" value="<?php echo ($vo["id"]); ?>"/>
		<div class="pageFormContent" layoutH="56">
			<p>
				<label>名称：</label>
				<input name="name" type="text" class="required" value="<?php echo ($vo["name"]); ?>"/>
			</p>
			<p>
				<label>代码名称：</label>
				<?php echo ($vo["menu"]); ?>
			</p>
			<p>
				<label>序列号：</label>
				<input name="sort" class="required" type="text"  value="<?php echo ($vo["sort"]); ?>"/>
			</p>
			
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>